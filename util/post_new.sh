#!/bin/bash

# configuration variables
GIT_ROOT=$(git rev-parse --show-toplevel)
POSTS_DIR="${GIT_ROOT}/app/_posts"

# defaults for arguments
POST_TITLE="No title"
TAGS_FILE="${GIT_ROOT}/util/generic.tags"
POST_SKELETON_FILE="${GIT_ROOT}/util/post-skeleton.md"
POST_DATE=$(date +"%Y-%m-%d")

function print_help()
{
    echo "usage: $0 [OPTION]"
    echo ""
    echo "-h          Print this text"
    echo "-s OPT      Skeleton file. Default: ${POST_SKELETON_FILE}"
    echo "-t OPT      Post title. Default: ${POST_TITLE}."
    echo "-a OPT      Tags file. Must be a bash file containing a TAGS variable. Default: ${TAGS_FILE}"
    echo "-d OPT      Post date, in YYYY-MM-DD format. Default: today (i.e. ${POST_DATE})"
}


# Parse arguments
while getopts ":hs:t:a:d:" opt; do
    case ${opt} in
        s )
            POST_SKELETON_FILE=$OPTARG
            ;;
        t )
            POST_TITLE=$OPTARG
            ;;
        a )
            TAGS_FILE=$OPTARG
            ;;
        d )
            POST_DATE=$OPTARG
            ;;
        h )
            print_help
            exit 1
            ;;
        \? )
            echo "Invalid option: -$OPTARG" 1>&2
            print_help
            exit 1
            ;;
        : )
            echo "Invalid option: -t$OPTARG requires an argument" 1>&2
            print_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

# vars derived from arguments
POST_URL_TITLE=$(echo "${POST_TITLE}" | tr '[:upper:]' '[:lower:]' | tr -d '[:punct:]' |sed -e "s/\ /-/g")
POST_FILE="${POSTS_DIR}/${POST_DATE}-${POST_URL_TITLE}.md"
source ${TAGS_FILE} || exit 1

echo -e "Creating post."
echo -e "\tDestination: ${POST_FILE}\n\tSkeleton: ${POST_SKELETON_FILE}\n"
echo -e "\tTitle: ${POST_TITLE}\n\tDate: ${POST_DATE}\n\tTags: ${TAGS}"
sed -e "s/\${TITLE}/${POST_TITLE}/" \
	-e "s/\${TAGS}/${TAGS}/" \
	"${POST_SKELETON_FILE}" > "${POST_FILE}"
