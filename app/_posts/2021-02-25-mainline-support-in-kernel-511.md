---
layout: post
title:  "Mainline support in kernel 5.11"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers newproductlaunch linux
---
## Where were we?

![First build](/assets/posts/TuxFlat.svg){: .center-image }
By gg3po, Iwan Gabovitch - Tux Flat SVG, based on File:NewTux.svg by gg3po which is based on File:Tux.png by Larry Ewing, GPL, https://commons.wikimedia.org/w/index.php?curid=48629023

In case you missed the [latest update]({% post_url 2020-11-12-tftp-boot %}), Elimo designed a small, 
powerful Linux System-on-Module, designed for low cost and fast system integration. It's FOSS and OSHW!

We have [buildroot](https://buildroot.org/) working and booting, and we are in the process of adding support for all 
peripherals and mainlining it.

# TL;DR

The name of our Allwinner S3 based module is **Impetus**, it now has a working [buildroot](https://buildroot.org) tree
that you can find on [our public GitHub repo](https://github.com/elimo-engineering/buildroot/tree/elimo)!

Last week [version 5.11 of the Linux kernel was released](https://lore.kernel.org/lkml/CAHk-=wg8LpRi9+P2_V+ehJ2_EoJifNh2QmVf47aLXfkCd-1UAQ@mail.gmail.com/T/#u), 
and we're proud to say that it includes the [first patches](https://lkml.org/lkml/2020/10/28/1260) from our mainlining 
effort!

To receive updates about our work on the board and how to buy some, watch this space or subscribe to 
our [RSS feed](/feed.xml)!  
We also offer bespoke embedded electronics design services - [get in touch](/#contact) to find out how we
 can help to make your electronic product idea a reality

# The patchset

These patches are mostly about and generally adding support for Ethernet, serial1, putting the module and carrier board 
on the radar. 
As a side effect of the review on the patchset, they also introduce some minor tidy up in the `compatible`
strings for S3 based designs, including the Pine64 Pinecube.
Also, this adds UART1 support to the dts for the Allwinner V3, and therefore the S3.

# What this means for us

As you can imagine, the patchset being in the mainline kernel means that our buildroot repo does not need the patches
anymore, and we can directly bring in kernel 5.11.
You can find the commit doing exactly that [on our repo here](https://github.com/elimo-engineering/buildroot/commit/5af7b977a066791df97ba2293773449d5b7b84b2).

The next steps will be adding support for the audio codec and the Bluetooth radio on the module.


