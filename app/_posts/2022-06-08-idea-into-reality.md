---
layout: post
title:  "Turning Your Idea into Reality with Elimo"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers linux
---

![Take the plunge with Elimo](/assets/posts/doesnt_have_to_be_painful_blue.jpg "Take the plunge"){: .center-image }

Elimo aims to be a fun and inspiring company to work with. We believe in making great ideas come to life and making the journey from idea to reality a fast-paced, enjoyable and enlightening adventure. We don’t believe in process for its own sake, but putting some structure around projects is helpful for everyone to know where they are and what expectations are appropriate.

## Discovery
We especially enjoy hearing about the great ideas our clients have come up with. At this stage, we listen hard to what you need the project to accomplish, your ideas about how this will be brought about and get a clear idea of your priorities. As well as absorbing everything about our client’s ideas, listening hard also entails probing assumptions and finding creative ways to achieve the goals while managing down the risks and costs wherever possible. 

## Component Selection, Firmware Architecture
Once the goals are clear, we start to design the system around key components, typically the sensors required to generate the data needed, the communications for getting the data to wherever they need to go and the microcontroller to make the project hang together. We consult with our clients about the choices involved to ensure that the selection strikes a good balance between speed, cost and future proofing. In the current environment, 

## Platform Board, Outline Firmware
The purpose of the first prototype is to assemble all of the selected components on a board for the first time in order to iron out any wrinkles in their performance. We compare actual performance with our client’s needs and make any adjustments necessary. The platform board is typically larger and more accessible than the final board, enabling minor modifications to the circuitry and easy access to circuit elements for the time-consuming process of making measurements and implementing the functionality through firmware.

## Formfit Board
With all the elements working as anticipated, the final design is implemented on a new PCB shaped to fit the final enclosure. The functionality of the board and firmware are checked against the brief to ensure that they fully meet the client’s stated expectations.

## Finished Project
The most satisfying part of a project comes once everyone is happy with the performance of the design, and we implement a number of copies to the client’s instructions. In many cases, this means a handful of finished products with all of the design documents passed to the client for volume manufacture. 
Elimo has deep working relationships with manufacturers, and can oversee the manufacturing process on clients’ behalf if desired. Where appropriate for small volume orders, Elimo can also make limited runs by hand in its studio. 

Elimo will always be available should any issues arise after project completion, but generally our work is done at this point and we take pride in following the commercial success that our clients enjoy with the products that we have designed.
