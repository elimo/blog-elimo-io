---
layout: post
title:  "The Elimo Impetus is born"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers newproductlaunch linux
---
## Where were we?

![First build](/assets/posts/buildroot_build.png){: .center-image }

In case you missed the [latest update]({% post_url 2020-10-19-board-bringup %}), Elimo designed a small, 
powerful Linux System-on-Module, designed for low cost and fast system integration. It's FOSS and OSHW!

This week we received the first prototypes of both the module and its first carrier board, so we started work on getting
[buildroot](https://buildroot.org/) to boot on it!

# TL;DR

The name of our Allwinner S3 based module is **Impetus**, it now has a working [buildroot](https://buildroot.org) tree
that you can find on [our public GitHub repo](https://github.com/elimo-engineering/buildroot/tree/elimo)!

To receive updates about our work on the board and how to buy some, watch this space or subscribe to 
our [RSS feed](/feed.xml)!  
We also offer bespoke embedded electronics design services - [get in touch](/#contact) to find out how we
 can help to make your electronic product idea a reality

## A man needs a name

We couldn't stand calling this "the S3 board" any longer, so we thought long and hard about a name for this.
To be honest, we've been bouncing ideas around for weeks, but none seemed to stick: some were overused, some had
horrible SEO-ness (yes, that's a word now!) some were unpronounceable, some were ... 


Please welcome the **Impetus**!

Impetus, *noun*  
*Something that makes a process or activity happen, or happen more quickly.*

The idea was Simon's, and we liked it because it not only it checks all the boxes, but it also keeps with the Latin 
language theme of Elimo.

![First build](/assets/posts/som_animated.gif){: .center-image }

## DRAM, u-boot and all that jazz

As you can read in [our previous post]({% post_url 2020-10-19-board-bringup %}), mainline u-boot currently does not 
support the DDR3 DRAM on the S3; the S3 is very similar to the V3s, and you could *almost* use a mainline V3s build of
u-boot if it wasn't for that pesky, faster, bigger on-die RAM. What's the opposite of the expression "a blessing 
in disguise"? 

Luckily the FOSS universe is always helpful <imagine a "so proud of this community" meme here>: 
[PINE64](https://www.pine64.org/) has recently released the PineCube, based on the Allwinner S3L, and Daniel Fullmer
wrote a [NixOS recipe](https://github.com/danielfullmer/pinecube-nixos) for it, including 3 patches for u-boot.
They apply cleanly on u-boot 2020.10, so that's what we did.

We forked buildroot/master, added the patches, our board configuration and, with some polishing and tidy up, we got a nice 
console out of UART0.

The current defconfig is based on:
- mainline u-boot 2020.10 + patches
- mainline Linux 5.3.5
- a minimal rootfs

The whole thing boots in ~4 seconds, half of which is just u-boot waiting for human input and could be shaved off.
For those that want to see the full gory detail, this is a boot sequence right now:

```
U-Boot SPL 2020.10 (Oct 23 2020 - 15:37:41 +0100)
DRAM: 128 MiB
Trying to boot from MMC1


U-Boot 2020.10 (Oct 23 2020 - 15:37:41 +0100)- Elimo Engineering

CPU:   Allwinner V3s (SUN8I 1681)
Model: Elimo Impetus
DRAM:  128 MiB
MMC:   mmc@01c0f000: 0
Loading Environment from FAT... *** Warning - bad CRC, using default environment

In:    serial@01c28000
Out:   serial@01c28000
Err:   serial@01c28000
Net:   phy interface0
eth0: ethernet@1c30000
starting USB...
No working controllers found
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc0 is current device
Scanning mmc 0:1...
Found U-Boot script /boot.scr
293 bytes read in 3 ms (94.7 KiB/s)
## Executing script at 41900000
4170248 bytes read in 195 ms (20.4 MiB/s)
9041 bytes read in 6 ms (1.4 MiB/s)
## Flattened Device Tree blob at 41800000
   Booting using the fdt blob at 0x41800000
   Loading Device Tree to 42dfa000, end 42dff350 ... OK

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 5.3.5 (matteo@elimo) (gcc version 9.3.0 (Buildroot 2020.08-949-g36edacce9c-dirty)) #1 SMP Fri Oct 23 14:12:04 BST 2020
[    0.000000] CPU: ARMv7 Processor [410fc075] revision 5 (ARMv7), cr=10c5387d
[    0.000000] CPU: div instructions available: patching division code
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Lichee Pi Zero with Dock
[    0.000000] Memory policy: Data cache writealloc
[    0.000000] cma: Reserved 16 MiB at 0x46c00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: Using PSCI v0.1 Function IDs from DT
[    0.000000] percpu: Embedded 15 pages/cpu s30412 r8192 d22836 u61440
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 32512
[    0.000000] Kernel command line: console=ttyS0,115200 panic=5 console=tty0 rootwait root=/dev/mmcblk0p2 earlyprintk rw
[    0.000000] Dentry cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
[    0.000000] Inode-cache hash table entries: 8192 (order: 3, 32768 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] Memory: 103492K/131072K available (6144K kernel code, 431K rwdata, 1668K rodata, 1024K init, 242K bss, 11196K reserved, 16384K cma-reserved, 0K highmem)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.000000] rcu: Hierarchical RCU implementation.
[    0.000000] rcu:     RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=1.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=1
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] GIC: GICv2 detected, but range too small and irqchip.gicv2_force_probe not set
[    0.000000] random: get_random_bytes called from start_kernel+0x2f8/0x498 with crng_init=0
[    0.000000] arch_timer: cp15 timer(s) running at 24.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x588fe9dc0, max_idle_ns: 440795202592 ns
[    0.000007] sched_clock: 56 bits at 24MHz, resolution 41ns, wraps every 4398046511097ns
[    0.000020] Switching to timer-based delay loop, resolution 41ns
[    0.000221] clocksource: timer: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 79635851949 ns
[    0.000458] Console: colour dummy device 80x30
[    0.000792] printk: console [tty0] enabled
[    0.000848] Calibrating delay loop (skipped), value calculated using timer frequency.. 48.00 BogoMIPS (lpj=240000)
[    0.000879] pid_max: default: 32768 minimum: 301
[    0.001055] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.001088] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.001967] CPU: Testing write buffer coherency: ok
[    0.002528] /cpus/cpu@0 missing clock-frequency property
[    0.002581] CPU0: thread -1, cpu 0, socket 0, mpidr 80000000
[    0.003405] Setting up static identity map for 0x40100000 - 0x40100060
[    0.003656] rcu: Hierarchical SRCU implementation.
[    0.004188] smp: Bringing up secondary CPUs ...
[    0.004233] smp: Brought up 1 node, 1 CPU
[    0.004249] SMP: Total of 1 processors activated (48.00 BogoMIPS).
[    0.004265] CPU: All CPU(s) started in HYP mode.
[    0.004277] CPU: Virtualization extensions available.
[    0.005133] devtmpfs: initialized
[    0.008129] VFP support v0.3: implementor 41 architecture 2 part 30 variant 7 rev 5
[    0.008481] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.008535] futex hash table entries: 256 (order: 2, 16384 bytes, linear)
[    0.009349] pinctrl core: initialized pinctrl subsystem
[    0.010803] NET: Registered protocol family 16
[    0.012352] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.013623] hw-breakpoint: found 5 (+1 reserved) breakpoint and 4 watchpoint registers.
[    0.013672] hw-breakpoint: maximum watchpoint size is 8 bytes.
[    0.029302] SCSI subsystem initialized
[    0.030042] usbcore: registered new interface driver usbfs
[    0.030266] usbcore: registered new interface driver hub
[    0.030373] usbcore: registered new device driver usb
[    0.030629] pps_core: LinuxPPS API ver. 1 registered
[    0.030653] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.030690] PTP clock support registered
[    0.030982] Advanced Linux Sound Architecture Driver Initialized.
[    0.032313] clocksource: Switched to clocksource arch_sys_counter
[    0.043211] thermal_sys: Registered thermal governor 'step_wise'
[    0.043558] NET: Registered protocol family 2
[    0.044274] tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes, linear)
[    0.044337] TCP established hash table entries: 1024 (order: 0, 4096 bytes, linear)
[    0.044375] TCP bind hash table entries: 1024 (order: 1, 8192 bytes, linear)
[    0.044410] TCP: Hash tables configured (established 1024 bind 1024)
[    0.044573] UDP hash table entries: 256 (order: 1, 8192 bytes, linear)
[    0.044638] UDP-Lite hash table entries: 256 (order: 1, 8192 bytes, linear)
[    0.044904] NET: Registered protocol family 1
[    0.045830] RPC: Registered named UNIX socket transport module.
[    0.045882] RPC: Registered udp transport module.
[    0.045898] RPC: Registered tcp transport module.
[    0.045912] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.047970] workingset: timestamp_bits=30 max_order=15 bucket_order=0
[    0.056014] NFS: Registering the id_resolver key type
[    0.056097] Key type id_resolver registered
[    0.056114] Key type id_legacy registered
[    0.056242] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 248)
[    0.056271] io scheduler mq-deadline registered
[    0.056286] io scheduler kyber registered
[    0.057209] sun4i-usb-phy 1c19400.phy: Couldn't request ID GPIO
[    0.061020] sun8i-v3s-pinctrl 1c20800.pinctrl: initialized sunXi PIO driver
[    0.126477] Serial: 8250/16550 driver, 8 ports, IRQ sharing disabled
[    0.128910] sun8i-v3s-pinctrl 1c20800.pinctrl: 1c20800.pinctrl supply vcc-pb not found, using dummy regulator
[    0.130103] printk: console [ttyS0] disabled
[    0.150435] 1c28000.serial: ttyS0 at MMIO 0x1c28000 (irq = 32, base_baud = 1500000) is a U6_16550A
[    0.723100] printk: console [ttyS0] enabled
[    0.752623] libphy: Fixed MDIO Bus: probed
[    0.756764] CAN device driver interface
[    0.761079] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    0.767697] ehci-platform: EHCI generic platform driver
[    0.773087] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    0.779307] ohci-platform: OHCI generic platform driver
[    0.785691] input: 1c22800.lradc as /devices/platform/soc/1c22800.lradc/input/input0
[    0.794923] sun6i-rtc 1c20400.rtc: registered as rtc0
[    0.800014] sun6i-rtc 1c20400.rtc: RTC enabled
[    0.804831] i2c /dev entries driver
[    0.809567] sunxi-wdt 1c20ca0.watchdog: Watchdog enabled (timeout=16 sec, nowayout=0)
[    0.818348] sun8i-v3s-pinctrl 1c20800.pinctrl: 1c20800.pinctrl supply vcc-pf not found, using dummy regulator
[    0.855124] sunxi-mmc 1c0f000.mmc: initialized, max. request size: 16384 KB
[    0.862482] sun8i-v3s-pinctrl 1c20800.pinctrl: 1c20800.pinctrl supply vcc-pg not found, using dummy regulator
[    0.897315] sunxi-mmc 1c10000.mmc: initialized, max. request size: 16384 KB
[    0.905436] usbcore: registered new interface driver usbhid
[    0.911038] usbhid: USB HID core driver
[    0.917082] NET: Registered protocol family 17
[    0.921593] can: controller area network core (rev 20170425 abi 9)
[    0.928019] NET: Registered protocol family 29
[    0.932534] can: raw protocol (rev 20170425)
[    0.936817] can: broadcast manager protocol (rev 20170425 t)
[    0.942512] can: netlink gateway (rev 20170425) max_hops=1
[    0.948335] Key type dns_resolver registered
[    0.952841] Registering SWP/SWPB emulation handler
[    0.966191] usb_phy_generic usb_phy_generic.0.auto: usb_phy_generic.0.auto supply vcc not found, using dummy regulator
[    0.977816] musb-hdrc musb-hdrc.1.auto: MUSB HDRC host driver
[    0.983707] musb-hdrc musb-hdrc.1.auto: new USB bus registered, assigned bus number 1
[    0.993399] hub 1-0:1.0: USB hub found
[    0.997401] hub 1-0:1.0: 1 port detected
[    1.003049] sun6i-rtc 1c20400.rtc: setting system clock to 1970-01-01T00:13:43 UTC (823)
[    1.011509] ALSA device list:
[    1.014606]   No soundcards found.
[    1.018521] Waiting for root device /dev/mmcblk0p2...
[    1.023780] mmc0: host does not support reading read-only switch, assuming write-enable
[    1.033774] mmc0: Problem switching card into high-speed mode!
[    1.039820] mmc0: new SDHC card at address 0001
[    1.046315] mmcblk0: mmc0:0001 SD16G 3.75 GiB
[    1.053775]  mmcblk0: p1 p2
[    1.061311] mmc1: queuing unknown CIS tuple 0x80 (2 bytes)
[    1.068448] mmc1: queuing unknown CIS tuple 0x80 (3 bytes)
[    1.075493] mmc1: queuing unknown CIS tuple 0x80 (3 bytes)
[    1.084740] mmc1: queuing unknown CIS tuple 0x80 (7 bytes)
[    1.094870] mmc1: queuing unknown CIS tuple 0x81 (9 bytes)
[    1.109730] random: fast init done
[    1.114205] EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
[    1.122557] VFS: Mounted root (ext4 filesystem) on device 179:2.
[    1.130786] devtmpfs: mounted
[    1.135369] Freeing unused kernel memory: 1024K
[    1.140098] Run /sbin/init as init process
[    1.187960] mmc1: new high speed SDIO card at address 0001
[    1.295833] EXT4-fs (mmcblk0p2): re-mounted. Opts: (null)
[    1.479864] random: dd: uninitialized urandom read (512 bytes read)

Welcome to Buildroot for the Elimo Impetus
impetus login:
```


