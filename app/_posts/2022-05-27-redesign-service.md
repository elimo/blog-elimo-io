---
layout: post
title:  "Elimo Product Redesign Service"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers linux
---

![Elimo Engineering Redesign Service](/assets/posts/redesign_service.png "PCB layout by Elimo Engineering"){: .center-image }

In recent years, we’ve seen specific chip shortages manifesting out of the blue, sometimes making products impossible to manufacture. Equally, it has long been identified that products tend to have a natural life cycle. The length of the cycle varies greatly depending on the product, but for electronic hardware, the cycle tends to be a handful of years. 

Both of these elements bring about the need for product redesign, together with the risks and opportunities inherent in that process. Elimo’s services enable our clients to benefit from the opportunities and minimise the risks, letting you take your products forward as quickly and painlessly as possible.

## Component Replacement

Production can grind to a halt when a chip or other semiconductor around which your product has been designed becomes unavailable. Elimo can assist you in identifying alternative components that can be used to deliver functionality and reliability that make the product indistinguishable from the previous iteration, as viewed by the end user. This will typically involve an assessment of the required capabilities and identifying suitable semiconductors to take over the roles of the old devices. 

Once this is accomplished, the PCB on which the components are laid out will need at least some minor tweaking to accommodate the new component(s). Generally, it will be possible for us to produce a new design that follows the exact same form factor as the original design, so there will be no need to redesign the enclosure or other elements of the overall product. 

Finally, the new components need to be persuaded to co-operate in the same way as their predecessors. This could involve anything from a small tweak to get a new sensor integrated into the product or more extensive re-coding. If the microcontroller has been replaced, it may involve starting from our extensive portfolio of existing model code to get everything working as it should once more.

## Moving Through the Product Life Cycle
Products naturally move through their life cycle. For those wanting a refresher on the concept, here’s an interesting [article published in the HBR from 1965](https://hbr.org/1965/11/exploit-the-product-life-cycle) that promises to turn the idea “into a managerial instrument of competitive power”. 

Suffice to say for our purposes that as your understanding of your customers’ needs evolves, and as new electronic possibilities open up, you will want to improve your existing products to take advantage of these opportunities. Similarly, it is often appropriate to use the stimulus of a semiconductor shortage to incorporate the learnings of the past into a new iteration of the product.

Elimo's deep appreciation of state-of-the-art electronic and computing techniques enables us to advise our clients on how to optimise choices to incorporate these learnings and opportunities while minimising the risk of future component shortages. This enables our clients to take their business forward in a way that is as close to future-proof as can be in the fast-moving world of electronic products. 

Similar to the component replacement work outlined above, we will seek to understand the functionality of the current product and your desires for improvement to that functionality. Together, we will shape a new specification for the enhanced version of the device. Elimo will then take those specifications and deliver new designs and software to meet them, enabling you to embark on the next phase of your product journey.  
