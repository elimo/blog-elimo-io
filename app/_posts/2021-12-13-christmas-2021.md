---
layout: post
title:  "Merry Christmas from the Elimo Engineering team"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers linux
---

![Elimo Engineering PCB Tree](/assets/posts/elimo_xmas_tree.jpg "Elimo Engineering PCB Tree"){: .center-image }

It's been a rough couple of years for pretty much everyone, so this festive season I decided to enter the world of nonsense corporate gifts and try to bring a bit of fun and happiness in the process! I bring you… **the Elimo Engineering 2021 Christmas Tree**!

If you've ended up here because you've received one of these trees as a gift and are wondering what to do with it, the short answer is - connect the USB-C cable into the port at the back of the tree, and into any powered USB port, power supply, or power bank* and after a few seconds the tree will begin to display a colourful light show! 

If you wish, you can also write your own code for the tree using the Arduino™ IDE and program it directly via USB (more on that below).

<font size="1">*Some power banks may switch off automatically after a short period of time, this is a feature of the power bank and not a fault.</font>

We have produced a limited number of these trees, designed and assembled in our London studio by my own fair hands. We have given these as gifts to a select group of people this year, and therefore unfortunately these are not for sale (but by all means let us know if you would like this to change in the future). If you haven't been lucky enough to get one but you're here anyway, welcome - I hope you still find it an interesting project. 

The tree's main construction is a 2-layer, 1mm thick circuit board. The entire design had to be made using the normal circuit board manufacturing techniques but still look presentable, for example some of the 'baubles' on the front are made using silkscreen, others have the soldermask removed to expose the (rather pleasing) gold plated finish.

The front side has 18 APA102 (similar to WS2812B) addressable RGB LEDs, and on the back you'll find an ATTiny167 8-bit microcontroller, USB-C connector, and two buttons - RESET and MODE. By default the tree will run the FastLED library demo reel program and cycle through some patterns - the MODE button isn't implemented in the demo (an exercise for the reader perhaps :) ).

In other news, Elimo recently turned three years old! Happy birthday to us, and Merry Christmas to you!

-Simon R-M

![Elimo Tree GIF](/assets/posts/elimo_xmas_tree.gif "Elimo Engineering PCB Tree, animated"){: .center-image }

------------

Specs:

- Dimensions (HxWxD): 100mm x 84mm x 7mm
- Power: 5V @ 500mA (USB Type C)
- LEDs: APA102-2020-256-6 ([Datasheet](http://www.normandled.com/upload/202003/APA102-2020-6A%20LED%20Datasheet.pdf "Datasheet link"))
- Microcontroller: ATTiny167 ([Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8265-8-bit-AVR-Microcontroller-tinyAVR-ATtiny87-ATtiny167_datasheet.pdf "Datasheet link"))
- Schematics: ([PDF](/assets/posts/Elimo_Christmas_Tree_Schematic.pdf "Schematics PDF"))
- Gerbers: ([Zip](/assets/posts/Elimo_Christmas_Tree_Gerbers.zip "Gerbers Zip"))
- Demo Arduino sketch: ([.ino sketch](/assets/posts/DemoReel100_Elimo_Christmas_Tree.ino "Arduino Sketch"))

Images: [Imgur Link](https://imgur.com/a/Ir4nh80 "Imgur Link")


Programming Instructions:

- Install Arduino IDE: [External link](https://www.arduino.cc/en/software)
- Install ATTinyCore (via Arduino IDE boards manager): [External link](https://github.com/SpenceKonde/ATTinyCore/blob/master/Installation.md)
- Install Micronucleus driver: [External link](https://github.com/ArminJo/micronucleus-firmware#driver-installation)
- After installing, change the board type in the Arduino IDE to ATTinyCore ATTiny167, and check that the settings in the tools menu all match the image below:

![Arduino IDE Settings](/assets/posts/arduino_ide_settings.jpg "Arduino IDE Settings")

Now you can write your own Arduino sketches and upload them to the tree via the USB-C port. (Note: The USB data pins are only connected to one side of the USB-C, so if it doesn't work, flip the connector over.)
