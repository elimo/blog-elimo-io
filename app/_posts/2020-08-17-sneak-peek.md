---
layout: post
title:  "Sneak preview of Elimo Engineering's new product!"
tags: iot internetofthings iotdevices hardwaredesign hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers newproductlaunch linux sneakpeek
---
## A small, powerful Linux System-on-Module

![The PCB Layout](/assets/posts/som_pcb_layout.jpeg)

We are designing a small, powerful Linux System-on-Module, designed for low cost and fast system integration.

- M.2/NGFF 42mm Form Factor
- WiFi IEEE 802. 11abgn
- Bluetooth 4.2/BLE
- RGB LCD Interface
- MIPI Camera Interface
- IEEE 802. 3u Ethernet MAC+PHY
- USB2.0 (Host, Device, OTG)
- PWM, ADC, UART
- uSD, eMMC, SPI Flash Support
- Audio Line In/Out

More updates and release date to come - watch this space or subscribe to our [RSS feed](/feed.xml)!

We also offer bespoke embedded electronics design services - [get in touch](https://elimo.io#contact) to find out how we can help to make your 
electronic product idea a reality

![3D Render of the SoM](/assets/posts/som_animated.gif){: .center-image }
