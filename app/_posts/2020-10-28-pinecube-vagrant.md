---
layout: post
title:  "Vagrant and the PineCube SDK"
tags: embedded embeddedsystems embeddedlinux embeddedsystem maker makers linux
---
## The PineCube SDK

![Vagrant boot script](/assets/posts/vagrant.png){: .center-image }

In case you missed the [latest update]({% post_url 2020-10-23-buildroot-support %}), Elimo designed the Impetus, a small, 
powerful Linux System-on-Module, designed for low cost and fast system integration. It's FOSS and OSHW!

The Impetus is based on the Allwinner S3, an SoC which it shares with the Pine64 PineCube. As as result, we have joined 
the PineCube community and started sharing code.

An interesting part of this is the [PineCube SDK](https://wiki.pine64.org/index.php?title=PineCube), which (I think) comes 
from the Sochip SDK. The first reason the PineCube SDK is interesting is that it is publicly available: the Sochip SDK
is only available privately from Sochip, and they require you to buy their (not cheap) development board to have
access to it.
Despite being based on an old 3.x kernel (with heavy patching by Allwinner) the SDK has been and still is the source of 
a lot of information about the S3.
Because of that, even if you don't want to use it and be tied to such an old, vendor-customised kernel, it's useful to be able to 
build it and test.  

# The problem...

Unfortunately, the build system of the SDK is a bit outdated as are its dependencies.
Some of the dependencies are available on Ubuntu 16.04, but not on more recent versions and even there,
you still need to bring in even older versions of Make (3.82) and a specific version of Java JDK 6. 
Our build server and development machines are all based on either Debian Buster or Bullseye. That means 
that we had the choice of either bringing in a lot of old versions of packages (and probably running into funny 
build issues on each of them!) or setting up a virtual machine.

## ... and the solution

We could have created a VirtualBox or VMPlayer VM on our build server and used that, but it would not have been 
very community friendly, would it?
It also would have been not very reproducible and hard to maintain.

So we decided to create a Vagrant file based on the requirements from the PineCube wiki, and add a bootscript to
install all the required dependencies.
This means that anybody with a Vagrant installation can clone our repo and have a working development environment
for the SDK in a matter of minutes; also, any issues will be reproducible by others in the community. This should
avoid "but it works on my machine" kind of situations.

You can get the repo and instructions to setup and use it from 
our [public GitHub account](https://github.com/elimo-engineering/pinecube-sdk-vagrant).

## Finally...
To receive updates about our work, watch this space or subscribe to our [RSS feed](/feed.xml)!  
We also offer bespoke embedded electronics design services - [get in touch](/#contact) to find out how we
 can help to make your electronic product idea a reality