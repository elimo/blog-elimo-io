---
layout: post
title:  "Prototypes of the S3 module and breakout board are in!"
tags: iot internetofthings iotdevices hardwaredesign hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers newproductlaunch linux
---
## The prototypes are in!

![3D Render of the SoM](/assets/posts/som_animated.gif){: .center-image }

In case you missed the [latest update]({% post_url 2020-08-17-sneak-peek %}), Elimo is designing a small, 
powerful Linux System-on-Module, designed for low cost and fast system integration.

- M.2/NGFF 42mm Form Factor
- WiFi IEEE 802. 11abgn
- Bluetooth 4.2/BLE
- RGB LCD Interface
- MIPI Camera Interface
- IEEE 802. 3u Ethernet MAC+PHY
- USB2.0 (Host, Device, OTG)
- PWM, ADC, UART
- uSD, eMMC, SPI Flash Support
- Audio Line In/Out

## First smoke test

We just received the first samples from our prototyping run. These include both the M2 module itself and a breakout 
board that as been designed as an aid for development - think of the two together as a DevKit.

![S3 Module](/assets/posts/s3-module.jpg){: .center-image }

The boards power up and do not let out any of the [magic smoke](http://www.catb.org/jargon/html/M/magic-smoke.html), so 
the next step is board bring up!
A first quick build of u-boot 2019.10 got the core up and running, but the DRAM is not detected. 
Just for the sake of curiosity, this is the output from U-Boot at the moment.

```
U-Boot SPL 2019.10 (Oct 19 2020 - 15:57:38 +0100)
DRAM: 0 MiB
### ERROR ### Please RESET the board ###
```


This is probably due to the DRAM being configured for the V3s, which is not REALLY the same thing as an S3 with half the DRAM. While the V3s comes
with 512Mbit of DDR2, the S3 has 1024Mbit of DDR3 in the package, making the timings completely different.

![S3 Module on Breakout board](/assets/posts/s3-breakout.jpg){: .center-image }


To receive updates about the board bring up, watch this space or subscribe to our [RSS feed](/feed.xml)!

We also offer bespoke embedded electronics design services - [get in touch](https://elimo.io#contact) to find out how we
 can help to make your electronic product idea a reality

