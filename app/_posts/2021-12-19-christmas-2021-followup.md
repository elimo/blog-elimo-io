---
layout: post
title:  "Great feeback on the Elimo Christmas Tree =)"
tags: iot internetofthings iotdevices hardwaredesign buildroot hardware electronicsengineering electronics embedded embeddedsystems b2b sme embeddedlinux embeddedsystem som wifi bluetooth ble bluetoothlowenergy arm maker makers linux
---

![Elimo Engineering PCB Tree Drawing](/assets/posts/TreeDrawing.JPG "Elimo Engineering PCB Tree Drawing"){: .center-image }

After [our recent post]({% post_url 2021-12-13-christmas-2021 %}), the response has been incredible, with some of 
you showing interest in making your own Christmas trees!

We couldn't let this go unrewarded, so we have decided to publish the gerbers for this little project, in the 
hope that they can make somebody's Christmas a bit more... blinkenlight-y!

You can find the gerbers in the updated "Specs" section below.

------------

Specs:

- Dimensions (HxWxD): 100mm x 84mm x 7mm
- Power: 5V @ 500mA (USB Type C)
- LEDs: APA102-2020-256-6 ([Datasheet](http://www.normandled.com/upload/202003/APA102-2020-6A%20LED%20Datasheet.pdf "Datasheet link"))
- Microcontroller: ATTiny167 ([Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8265-8-bit-AVR-Microcontroller-tinyAVR-ATtiny87-ATtiny167_datasheet.pdf "Datasheet link"))
- Schematics: ([PDF](/assets/posts/Elimo_Christmas_Tree_Schematic.pdf "Schematics PDF"))
- Gerbers: ([Zip](/assets/posts/Elimo_Christmas_Tree_Gerbers.zip "Gerbers Zip"))
- Demo Arduino sketch: ([.ino sketch](/assets/posts/DemoReel100_Elimo_Christmas_Tree.ino "Arduino Sketch"))

Images: [Imgur Link](https://imgur.com/a/Ir4nh80 "Imgur Link")


Programming Instructions:

- Install Arduino IDE: [External link](https://www.arduino.cc/en/software)
- Install ATTinyCore (via Arduino IDE boards manager): [External link](https://github.com/SpenceKonde/ATTinyCore/blob/master/Installation.md)
- Install Micronucleus driver: [External link](https://github.com/ArminJo/micronucleus-firmware#driver-installation)
- After installing, change the board type in the Arduino IDE to ATTinyCore ATTiny167, and check that the settings in the tools menu all match the image below:

![Arduino IDE Settings](/assets/posts/arduino_ide_settings.jpg "Arduino IDE Settings")

Now you can write your own Arduino sketches and upload them to the tree via the USB-C port. (Note: The USB data pins are only connected to one side of the USB-C, so if it doesn't work, flip the connector over.)
