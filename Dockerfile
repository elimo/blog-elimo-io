FROM ruby:2.7-alpine

ENV JEKYLL_ENV: production
ENV LC_ALL: C.UTF8
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

# copy the Gemfile first, so we can install the gems bundle before the app
# by using the Docker cache, this avoids having to reinstall all 
# bundles each time, which is slow
COPY app/Gemfile* ./

# install the pre-requirements (plus a couple of steps to keep apache happy)
RUN adduser -D elimouser && \
    apk add --no-cache apache2 apache2-utils g++ make && \
    gem install bundler && \
    bundle install && \
    echo "ServerName localhost" >> /etc/apache2/httpd.conf && \
    mkdir -p /run/apache2 && \
    mkdir app

# copy the app
WORKDIR app
COPY app/. ./
# generate the site
RUN bundle exec jekyll build -d /var/www/localhost/htdocs
WORKDIR ../

# these were only needed during bundle install && site generation
RUN apk del g++ make && rm -rf app /usr/local/bundle

#USER elimouser

EXPOSE 80
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
